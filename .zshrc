# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob nomatch notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/scott/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

autoload -Uz promptinit
promptinit
prompt adam2

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

alias ls='ls --color=auto'
alias l='ls -XphG'
alias dot='git --git-dir=$HOME/.dot/ --work-tree=$HOME'
alias list-packages='pacman -Qqettn'
alias e='vim'

PATH=$PATH:.
export EDITOR=vim

archey3 --config=$HOME/.config/archey3.cfg
