" Don't want vi compatibility
set nocompatible



" ============================================================================ "
" ================================= Plugins ================================== "
" ============================================================================ "

filetype off " Turn off for plugins
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" Tab completion
Plugin 'ervandew/supertab'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'tpope/vim-surround'
Plugin 'easymotion/vim-easymotion'
" Plugin 'lervag/vimtex'
" Plugin 'wlangstroth/vim-racket'
" Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'

" Better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit = "vertical"

call vundle#end()



" ============================================================================ "
" =========================== File and Filesystem ============================ "
" ============================================================================ "

" Enable filetype plugins
filetype plugin indent on

" Sandard filetype and backups
set ffs=unix,dos,mac
set encoding=utf8
let g:tex_flavor="tex"

" NOTE: NO BACKUPS!! Assumes using git etc. instead!
set noswapfile
set nobackup
set writebackup

" Per file settings
autocmd FileType tex setlocal spell
autocmd FileType tex set complete+=kspell
autocmd FileType tex set shiftwidth=2
autocmd FileType tex set tabstop=2
autocmd FileType tex vnoremap <buffer> <leader>" <esc>`>a''<esc>`<i``<esc>
autocmd FileType tex vnoremap <buffer> <leader>' <esc>`>a'<esc>`<i`<esc>
autocmd FileType tex inoremap <buffer> \[ \[\]<left><left>
autocmd FileType tex inoremap <buffer> \{ \{\}<left><left>
autocmd FileType tex noremap <buffer> <leader>r :w<cr>:!
            \latexmk -lualatex -interaction=nonstopmode %<cr>

autocmd FileType python noremap <buffer> <leader>r :w<cr>:!python %<cr>
autocmd FileType d map <buffer> <leader>= m":%!dfmt --brace_style=otbs --max_line_length=80 2> /dev/null<cr>`"

autocmd FileType lua set noexpandtab
autocmd FileType lua set shiftwidth=3
autocmd FileType lua set tabstop=3
" ...

" Remeber where we were when last editing a file
autocmd BufReadPost * 
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif
set viminfo^=%

" NetRW file browser settings
let g:netrw_banner=0
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " split to right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide+=',\(^\|\s\s\)\zs\.\S\+'



" ============================================================================ "
" ============================= General Settings ============================= "
" ============================================================================ "

set history=700     " How many lines of history
set autoread        " Reload when file is changed externally
set cmdheight=1
set hidden          " Hide buffer when closed
set showcmd         " Show commands as you type

set incsearch       " Incremental search
set hlsearch        " Highlight search matches; press <esc><esc> to stop

" Match brackets
set showmatch
set matchtime=2

" No error feedback
set noerrorbells
set novisualbell
set t_vb=

" Timeout while waiting for key sequences
set timeoutlen=500

" Extra options for gui
if has("gui_running")
    set guioptions-=T
    set guioptions+=e
    " set guitablable=%M\ %t
    set guifont=Inconsolata\ 13
endif

" Copy/Paste from clipboard
if has('unnamedplus')
    set clipboard=unnamedplus
endif



" ============================================================================ "
" ============================= Text and Indent ============================== "
" ============================================================================ "

set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

set autoindent
set smartindent
set wrap
set linebreak
set breakindent
set textwidth=80

" Give us nice EOL (end of line) characters
set listchars=tab:>-,space:‧,eol:¬,trail:-,extends:>,precedes:<
" set list      " Used for showing whitespace chars



" ============================================================================ "
" ========================== Windows and Interface =========================== "
" ============================================================================ "

set scrolloff=12
set number
set laststatus=2
set statusline=\ %0.32F\ %y%m%r%h\ %w%=CWD:\ %.25{getcwd()}\ \ \ Line:%4l/%L\ Col:%3c\ \ 

set wildmenu
set wildignore=*.o,*~,*.pyc,*.class,*.toc,*.aux,*.blg,*.log,*.bbl,*.lof,*.pdf
set path+=**    " Search subfolders

" Colors
colorscheme vimbrant
set background=dark
set t_Co=256
syntax enable



" ============================================================================ "
" ================================= Bindings ================================= "
" ============================================================================ "

" Useful for custom mappings
let mapleader=","

" Stop highlighting search
nnoremap <silent> <esc><esc> <esc>:nohlsearch<cr>:set nolist<cr>
nnoremap <silent> <leader>r :w<cr>:!%<cr>
nnoremap <silent> <leader>w :w<cr>
nnoremap <silent> <c-s> :w<cr>

" Fuck F1
noremap <F1> <nop>
inoremap <F1> <nop>

" Stop being lazy!
inoremap <left> <nop>
inoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>

noremap <left> <nop>
noremap <right> <nop>
noremap <up> <nop>
noremap <down> <nop>

" trying to navigate in insert mode exits insert mode
inoremap hh <esc>
inoremap jj <esc>
inoremap kkk <esc>
inoremap lll <esc>

" Navigating windows
map <c-h> <c-w>h
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l 

" Resizing windows, these mappings might not be portable between different
" terminals
map + <c-w>+
map - <c-w>-
map > <c-w>>
map < <c-w><

if has('terminal')
    tmap <c-h> <c-w>h
    tmap <c-j> <c-w>j
    tmap <c-k> <c-w>k
    tmap <c-l> <c-w>l 
    tmap + <c-w>+
    tmap - <c-w>-
    tmap > <c-w>>
    tmap < <c-w><
endif

" Bracket completion
inoremap {{ {
inoremap {<bs> <space><bs>
inoremap { {}<left>
inoremap {<cr> {<cr>}<up><end><cr>
inoremap } <c-r>=<SID>WalkOverOrInsert('}')<cr>
vnoremap <leader>{ <esc>`>a}<esc>`<i{<esc>

inoremap [[ [
inoremap [<bs> <space><bs>
inoremap [ []<left>
inoremap [<cr> [<cr>]<up><end><cr>
inoremap ] <c-r>=<SID>WalkOverOrInsert(']')<cr>
vnoremap <leader>[ <esc>`>a]<esc>`<i[<esc>

inoremap (( (
inoremap (<bs> <space><bs>
inoremap ( ()<left>
inoremap (<cr> (<cr>)<up><end><cr>
inoremap ) <c-r>=<SID>WalkOverOrInsert(')')<cr>
vnoremap <leader>( <esc>`>a)<esc>`<i(<esc>

vnoremap <leader>" <esc>`>a"<esc>`<i"<esc>
vnoremap <leader>' <esc>`>a'<esc>`<i'<esc>
inoremap ' <c-r>=<SID>WalkOverOrInsert("'")<cr>
inoremap " <c-r>=<SID>WalkOverOrInsert('"')<cr>

" Just moves right once if the next char is already a:char
function! s:WalkOverOrInsert(char)
    if getline('.')[col('.') - 1] == a:char
        return "\<right>"
    else
        return a:char
    endif
endf

